const matches = require('../data/matches.json');
const deliveries = require('../data/deliveries.json');

// Number of times team won toss and also won the match
function wonTossAndMatch(matches) {

    let tossAndMatch = {}

    for (let i = 0; i < matches.length; i++) {
        if (tossAndMatch.hasOwnProperty(matches[i].toss_winner)) {
            if (matches[i].toss_winner == matches[i].winner) {
                tossAndMatch[matches[i].winner] += 1
            }
        }
        else {
            if (matches[i].toss_winner == matches[i].winner) {
                tossAndMatch[matches[i].winner] = 1
            }
        }
    }

    return tossAndMatch
};


// Highest number of Player of the Match awards for each season

function awardesPlayers(matches) {

    let playersAndNoOfAwards = {}

    for (let i = 0; i < matches.length; i++) {
        if (playersAndNoOfAwards.hasOwnProperty(matches[i].season)) {
            if (playersAndNoOfAwards[matches[i].season].hasOwnProperty(matches[i].player_of_match)) {
                playersAndNoOfAwards[matches[i].season][matches[i].player_of_match] += 1
            }
            else {
                playersAndNoOfAwards[matches[i].season][matches[i].player_of_match] = 1
            }

        }
        else {
            playersAndNoOfAwards[matches[i].season] = {}
        }
    }

    let sortedOrderOfAwards = Object.values(playersAndNoOfAwards)
        .map(nameAndNoOfAwards => Object.entries(nameAndNoOfAwards)
            .sort((current, previous) => current[1] - previous[1]));


    const topPlayers = sortedOrderOfAwards.map(nameAwards => nameAwards[nameAwards.length - 1])


    let mostAwardsPerYear = matches.reduce((mostAwardsPerYear, match) => {
        mostAwardsPerYear[match.season] = 0;
        return mostAwardsPerYear;
    }, {})

    Object.keys(mostAwardsPerYear).forEach((element, index) => {
        let name = topPlayers[index][0];
        let value = topPlayers[index][1];
        mostAwardsPerYear[element] = { [name]: value }
    })

    return mostAwardsPerYear;

}


// The strike rate of a batsman for each season

function strikeRate(deliveries, matches) {

    function getYearlyData(idArray) {

        let yearObj = deliveries.filter((obj) => idArray.includes(obj["match_id"]))

        let batsmanArray = [];

        for (let obj of yearObj) {
            if (batsmanArray.includes(obj['batsman'])) {
                continue;
            }
            else {
                batsmanArray.push(obj['batsman'])
            }
        }

        let batsmanObj = {}

        for (let batsman of batsmanArray) {
            let runs = 0
            let balls = 0
            for (let obj of yearObj) {
                if (batsman == obj['batsman']) {
                    runs += Number(obj['batsman_runs'])
                    if (obj['wide_runs'] == 0) {
                        balls += 1
                    }
                }
            }
            batsmanObj[batsman] = ((runs / balls) * 100).toFixed(2);
        }

        return batsmanObj


    }

    let seasonData = {}

    for (let i = 0; i < matches.length; i++) {
        if (seasonData.hasOwnProperty(matches[i].season)) {
            seasonData[matches[i].season].push(matches[i].id)

        }
        else {
            seasonData[matches[i].season] = [matches[i].id]
        }
    }

    let seasonArray = Object.entries(seasonData)

    for (let i = 0; i < seasonArray.length; i++) {
        seasonData[seasonArray[i][0]] = getYearlyData(seasonArray[i][1])
    }

    return seasonData;
}

// Number of times one player has been dismissed by another player

function playerDismissed(deliveries) {

    let combArray = []

    for (let i = 0; i < deliveries.length; i++) {
        if (deliveries[i]['player_dismissed'].length > 1) {
            combArray.push([deliveries[i]['batsman'], deliveries[i]['bowler']])
        }
    }

    let countArray = [];

    for (let comb of combArray) {
        let batsman = comb[0]
        let bowler = comb[1]
        let count = 0;
        for (let pair of combArray) {
            if (pair[0] == batsman && pair[1] == bowler) {
                count += 1
            }
        }
        countArray.push(count)
    }

    let maxDismissel = countArray.reduce((a, b) => Math.max(a, b))

    let indexArray = []

    for (let i = 0; i < countArray.length; i++) {
        if (countArray[i] == maxDismissel) {
            indexArray.push(i)
        }
    }

    let output = {}

    for (let index of indexArray) {

        batsman = combArray[index][0]
        bowler = combArray[index][1]

        let key = (batsman + "," + bowler)

        if (output.hasOwnProperty(key)) {
            continue
        }
        else {
            output[key] = maxDismissel
        }


    }

    return output;
}


//  bowler with the best economy in super overs

function economyBowler(deliveries){

    let bowelersArray = []

    for(let i=0; i<deliveries.length; i++){
        if(deliveries[i]['is_super_over']==1){
            if(bowelersArray.includes(deliveries[i]['bowler'])){
                continue;
            }
            else{
                bowelersArray.push(deliveries[i]['bowler']);
            }
        }
    }

    let outputObj = {}

    for(let bowler of bowelersArray){

        let runs = 0;
        let balls = 0;

        for(let obj of deliveries){
            if(obj['bowler']==bowler && obj['is_super_over']==1){
                runs += Number(obj['total_runs']-obj['bye_runs']-obj['legbye_runs'])
                if(obj['wide_runs']==0 && obj['noball_runs']){
                    balls += 1
                }
            }
        }

        const economy = Number((runs/(balls/6)).toFixed(2))

        if(outputObj.hasOwnProperty(bowler)){
            continue
        }
        else{
            outputObj[bowler] = economy;
        }

    }

    let outputArray = Object.entries(outputObj)

    let output = outputArray.sort((a,b) => a[1] - b[1])

    
    let economyBowlerObj = {}

    economyBowlerObj[output[0][0]] =  output[0][1]

   
    return economyBowlerObj
}


module.exports = { wonTossAndMatch, awardesPlayers, strikeRate, playerDismissed,economyBowler };

