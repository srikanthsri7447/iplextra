const matches = require('../data/matches.json');
const deliveries = require('../data/deliveries.json');
const iplFunctions = require('./ipl');
const fs = require('fs');

const bothTossAndMatchOwn = iplFunctions.wonTossAndMatch(matches);
const filePath1 = ('../public/output/teamWonTossAndMatch.json');
outputJson(filePath1, bothTossAndMatchOwn);

const playerOfMatch = iplFunctions.awardesPlayers(matches)
const filePath2 = '../public/output/playerAwarded.json'
outputJson(filePath2, playerOfMatch)

const strikeRateOfPlayers = iplFunctions.strikeRate(deliveries, matches)
const filePath3 = '../public/output/batsmanStrikeRate.json'
outputJson(filePath3, strikeRateOfPlayers)

const dismissedPlayers = iplFunctions.playerDismissed(deliveries);
const filePath4 = '../public/output/batsmanDismissel.json'
outputJson(filePath4, dismissedPlayers)

const bowelersEconomy = iplFunctions.economyBowler(deliveries);
const filePath5 = '../public/output/economyBowler.json'
outputJson(filePath5,bowelersEconomy)


function outputJson(filePath1, fileName) {
    fs.writeFile(filePath1, JSON.stringify(fileName), (err) => {
        if (err) console.log(err)
    });
};


