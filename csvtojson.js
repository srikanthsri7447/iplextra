const csv = require('csvtojson');

const csvFilePath1 = 'src/data/matches.csv';
const csvFilePath2 = 'src/data/deliveries.csv';

const jsonFilePath1 = 'src/data/matches.json';
const jsonFilePath2 = 'src/data/deliveries.json'


function csvToJson(filePath,fileName){
    csv()
    .fromFile(filePath)
    .then((jsonObj)=>{  
        try {
            return JSON.stringify(jsonObj)
        } catch (err) {
            console.log(err)
            
        }
    })
}

csvToJson(csvFilePath1,jsonFilePath1);
csvToJson(csvFilePath2,jsonFilePath2);
